<?php

return [
    'api_uri' => env('REST_SERVER', 'http://bsapidev.cpbonline.co.za'),
    'bureau_name' => env('REST_BUREAU_NAME', "APITEST"),
    'vendor' => env('REST_VENDOR', 'BureauHouse'),
    'user_entity' => '\\BureauHouse\\Modules\\Core\\Entities\\User',
    'components' => [
        'token' => [
            'uri' => 'token/token',
            'module' => 'Integration',
            'form_params' => [
                ''
            ]
        ],
        'validate' => [
            'uri' => 'token/validate',
            'module' => 'Integration',
            'form_params' => [
                ''
            ]
        ],
        'permissions' => [
            'uri' => 'token/listroles',
            'module' => 'Integration',
            'form_params' => [],
        ]
    ]

];
