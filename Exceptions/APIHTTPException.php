<?php

namespace BureauHouse\Modules\AirQuest\Exceptions;

use Exception;

class APIHTTPException extends Exception
{

    public function report()
    {
        //
    }

    public function render($request)
    {
        if ($request->expectsJson()) {
            return $this->ajax();
        }

        return response()->view('airquest::errors.api', [], 500);
    }

    public function ajax()
    {
        return response()->json(
            [
                'message'=> $this->getMessage(),
                'errors'=>[]
            ],
            intval($this->code)
        );
    }

    public function withData(array $data)
    {
        $this->data = $data;

        return $this;
    }

    public function withStatus($status)
    {
        $this->status = $status;

        return $this;
    }
}
