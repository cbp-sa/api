<?php

namespace BureauHouse\Modules\AirQuest\Exceptions;

use Exception;

class LoginBlockedException extends Exception
{

    private $isp;

    public function __construct($isp)
    {
        $this->isp = $isp;
    }

    public function report()
    {
    }

    public function render($request)
    {
        $email = null;
        $telephone = null;
        $parameters = [
            'IP Address' => $request->ip(),
            'ISP' => $this->isp,
            'Browser' => $request->header('User-Agent'),
        ];
        if ($company = config('company')) {
            $telephone = $company['Tel'] ?? $company['Tel'];
            $email = $company['EmailAccess'] ?? $company['EmailAccess'];
        }

        return response()->view('airquest::errors.blocked', compact('parameters', 'telephone', 'email'), 500);
    }
}
