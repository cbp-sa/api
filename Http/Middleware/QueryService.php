<?php

namespace BureauHouse\Modules\AirQuest\Http\Middleware;

use BureauHouse\Modules\AirQuest\Http\WebService;
use Closure;
use Illuminate\Http\Request;

class QueryService
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);
        $data = $response->getData();
        if (!property_exists($data, 'section')) {
            return $response;
        }
        if (!property_exists($data, 'module')) {
            return $response;
        }
        $section = $data->section;
        $module = $data->module;
        if (!(property_exists($section, 'name') && property_exists($section, 'url'))) {
            return $response;
        }
        $parameters = (array) $data->parameters;
        $webservice = WebService::getInstance();
        $key = md5($section->uniqueKey.$webservice->getToken());
        $result = $webservice->get($section->name, $section->url, $parameters);
        $response->setData([
            'section' => (array) $section,
            'module' => (array) $module,
            'result' => $result,
            'key' => $key
        ]);

        return $response;
    }
}
