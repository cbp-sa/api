<?php

namespace BureauHouse\Modules\AirQuest\Http;

use Carbon\Carbon;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

use BureauHouse\Modules\AirQuest\Exceptions\APIHTTPException;
use BureauHouse\Modules\AirQuest\Exceptions\LoginBlockedException;

class WebService
{
    const GET = 'GET';
    const POST = 'POST';

    private $connection;
    private $token;
    private $count = 0;

    /**
     * @var self|null
     */
    private static $instance;

    private function __construct()
    {
        $this->connection = new Client([
            'base_uri' => config('airquest.api_uri'),
            'verify' => false,
        ]);
    }

    /**
     * get user token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token ? $this->token : Auth::user()->token;
    }

    /**
     * Validate token
     *
     * @param string|null $token
     * @return array
     */
    public function validate(?string $token = null)
    {
        if (is_null($token)) {
            $token = Auth::user()->token;
        }
        $response = [
            'valid' => false,
            'until' => 0,
            'result' => []
        ];
        $result = $this->check($token);
        $response['valid'] = $result->getBody()->getContents() == 'OK';
        $response['until'] = Auth::user()->tokenTimeout;
        $response['result'] = [
            'code' => $result->getStatusCode(),
            'content' => $result->getBody()->getContents()
        ];

        return $response;
    }

    public function get(string $name, string $uri, array $parameters)
    {
        $response = $this->request(
            self::POST,
            $uri,
            [
                'form_params' => $this->getQuery($parameters)
            ]
        );
        $content = $response->getBody()->getContents();
        Log::info(config('airquest.api_uri') ."/{$uri}", $this->getQuery($parameters));
        /*if ($map == true) {*/
            return [
                'title' => $name,
                'data' => $content
            ];

        //}
    }

    public function truthy($token)
    {
        $result = $this->check($token);

        return $result->getBody()->getContents() == 'OK';
    }

    public function check($token)
    {
        $result = $this->request(
            self::POST,
            config('airquest.components.validate.uri'),
            [
                'form_params' => [
                    'Token' => $token,
                    'CallingModule' => config('airquest.components.validate.module')
                ]
            ]
        );

        return $result;
    }

    /**
     * Generate new token
     *
     * @return string
     */
    public function getNewToken()
    {
        $this->initialize(true)->truthy($this->token);

        return $this->token;
    }

    /**
     * login to API
     *
     * @param string $accountNumber
     * @param string $userCode
     * @param string $password
     * @param boolean $raw
     * @return AuthResult
     */
    public function login(string $accountNumber, string $userCode, string $password, $raw = false)
    {
        $result = $this->request(
            self::GET,
            config('airquest.components.token.uri'),
            [
                'query' => [
                    'AccountNumber' => $accountNumber,
                    'UserCode' => $userCode,
                    'Password' => $password,
                    'BureauName' => config('airquest.bureau_name'),
                    'VendorName' => config('airquest.vendor'),
                    'CallingModule' => config('airquest.components.token.module')
                ]
            ]
        );
        if ($raw) {
            return $result->getBody()->getContents();
        }


        $userClass = config('airquest.user_entity');
        $user = resolve($userClass);
        if ($user instanceof $userClass) {
            $user->token = $this->token = $result->getBody()->getContents();
            $user->tokenTimeout = Carbon::now()->addMinutes(10)->toDateTimeString();
            $user->status = $result->getStatusCode() == 200 && $this->truthy($user->token);
            $user->accountNumber = $accountNumber;
            $user->userCode = $userCode;
            $user->password = $password;
            $user->id = $accountNumber;
            $user->remember_token =  null;
            $user->permissions = $this->getPermissions();

            return $user;
        }

        return $result->getStatusCode() == 200;
    }

    protected function getPermissions()
    {
        $result = $this->request(
            self::GET,
            config('airquest.components.permissions.uri'),
            [
                'form_params' => [
                    'Token' => $this->getToken([]),
                    'CallingModule' => config('airquest.components.permissions.module')
                ]
            ]
        );

        return explode(', ', preg_replace('/"|,\W"$/', null, $result->getBody()->getContents()));
    }

    /**
     * get query to pass to API
     *
     * @param array $parameters
     * @return array
     */
    protected function getQuery(array $parameters)
    {
        return array_merge([
            'Token' => $this->getToken(),
        ], $parameters);
    }

    private function request($type, $uri, array $parameters = [])
    {
        try {
            return $this->connection->request($type, $uri, $parameters);
        } catch (ClientException $e) {
            if ($uri != config('airquest.components.token.uri') && $e->getResponse()->getStatusCode() == 401 && ++$this->count < 3) {
                Log::error($e->getMessage());
                $this->getNewToken();

                return $this->request($type, $uri, $parameters);
            }
            Log::error($e->getMessage());
            if (preg_match('/blocked/', $e->getMessage())) {
                throw new LoginBlockedException($this->getISP());
            }
        } catch (RequestException $e) {
            throw new APIHTTPException();
        }
    }

    /**
     * init can only be used once logged in, Auth guard must have a valid user.
     *
     * @param boolean $forceNewToken
     * @return self
     */
    private function initialize($forceNewToken = false)
    {
        if (!Auth::guest() || $forceNewToken) {
            $tokenTimeout =  Carbon::parse(Auth::user()->tokenTimeout);
            if ($tokenTimeout->lessThan(Carbon::now()) || $forceNewToken) {
                $result = $this->login(Auth::user()->accountNumber, Auth::user()->userCode, Auth::user()->password);
                $this->token = $result->token;
                $this->setToken($this->token, $result->tokenTimeout);
            } else {
                $this->token = Auth::user()->token;
            }
        }

        return $this;
    }

    /**
     * set user token
     *
     * @param string $token
     * @param string $timestamp
     * @return self
     */
    private function setToken(string $token, string $timestamp)
    {
        $identifier = base64_encode(Auth::user()->accountNumber);
        $sessionData = decrypt(session($identifier));
        $saveData = encrypt(array_merge($sessionData, ['token' => $token,'tokenTimeout' => $timestamp]));
        Session::put($identifier, $saveData);

        return $this;
    }

    private function getISP()
    {
        $matches = [];
        $content = $this->request(self::GET, 'https://www.whoismyisp.org/');
        preg_match('/(?<=isp">).*(?=<\/p)/', $content->getBody()->getContents(), $matches);

        return count($matches) > 0 ? end($matches) : null;
    }

    public static function getInstance()
    {
        if (self::$instance) {
            return self::$instance;
        }

        self::$instance = new self();

        return self::getInstance();
    }
}
