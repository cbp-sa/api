<?php

namespace BureauHouse\Modules\AirQuest\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Facades\Auth;

use BureauHouse\Modules\AirQuest\Http\Middleware\QueryService;

class AirQuestServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path('AirQuest', 'Database/Migrations'));
        $router = $this->app['router'];
        $router->aliasMiddleware('query.service', QueryService::class);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(BureauServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('AirQuest', 'Config/config.php') => config_path('airquest.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('AirQuest', 'Config/config.php'),
            'airquest'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/airquest');

        $sourcePath = module_path('AirQuest', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/airquest';
        }, \Config::get('view.paths')), [$sourcePath]), 'airquest');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/airquest');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'airquest');
        } else {
            $this->loadTranslationsFrom(module_path('AirQuest', 'Resources/lang'), 'airquest');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('AirQuest', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
