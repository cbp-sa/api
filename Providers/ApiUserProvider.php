<?php

namespace BureauHouse\Modules\AirQuest\Providers;

use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Exception\ClientException;

use BureauHouse\Modules\AirQuest\Http\WebService;

class ApiUserProvider implements UserProvider
{
    protected $hasher;

    /**
     * @var WebService
     */
    private $webservice;

    public function __construct(HasherContract $hasher, WebService $webservice)
    {
        $this->hasher = $hasher;
        $this->webservice = $webservice;
    }

    public function retrieveByCredentials(array $credentials)
    {
        $user = [];
        try {
            $user = $this->webservice->login(
                $credentials['accountNumber'],
                $credentials['userCode'],
                $credentials['password']
            );
        } catch (ClientException $e) {
            Log::error($e);
            throw $e;
        }

        if ($user->isLogged()) {
            $saveData = encrypt([
                'accountNumber' => $user->accountNumber,
                'userCode' => $user->userCode,
                'password' => $user->password,
                'id' => $user->id,
                'token' => $user->token,
                'tokenTimeout' => $user->tokenTimeout,
                'remember_token' => $user->remember_token,
                'permissions' => $user->permissions
            ]);

            Session::put(base64_encode($user->id), $saveData);
        }

        $user = $user ? : null;

        return $this->getApiUser($user);
    }

    public function retrieveById($identifier)
    {
        $user = null;
        $identifier = base64_encode($identifier);

        if (session()->has($identifier)) {
            $sessionData = decrypt(session($identifier));
            $userClass = config('airquest.user_entity');
            $user = resolve($userClass);
            if ($user instanceof $userClass) {
                $user->accountNumber =  $sessionData['accountNumber'];
                $user->userCode =  $sessionData['userCode'];
                $user->password =  $sessionData['password'];
                $user->id =  $sessionData['id'];
                $user->token =  $sessionData['token'];
                $user->tokenTimeout = $sessionData['tokenTimeout'];
                $user->remember_token =  $sessionData['remember_token'];
                $user->permissions = $sessionData['permissions'];
            }
        }

        return $this->getApiUser($user);
    }

    public function validateCredentials(UserContract $user, array $credentials)
    {
        return $credentials['password'] === $user->getAuthPassword();
    }

    protected function getApiUser($user)
    {
        if ($user !== null) {
            return $user;
        }
    }

    protected function getUserById($identifier)
    {
        $user = session()->get(base64_encode($identifier));

        return $user ?: null;
    }

    public function retrieveByToken($identifier, $token)
    {
    }

    public function updateRememberToken(UserContract $user, $token)
    {
    }
}
