<?php
namespace BureauHouse\Modules\AirQuest\Providers;

use BureauHouse\Modules\AirQuest\Http\WebService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class BureauServiceProvider extends ServiceProvider
{
    protected $policies = [];

    public function boot()
    {
        $this->registerPolicies();
        Auth::provider('bureau', function ($app, array $config) {
            return new ApiUserProvider($this->app['hash'], WebService::getInstance());
        });
    }
}
