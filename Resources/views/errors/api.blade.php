@extends(config('template.layout'))

@section('javascript')
    <link rel="stylesheet" href="{{ mix('css/airquest.css') }}">
@endsection

@section('baseContent')
    <div class="airquest container text-center">
        <div class="alert alert-danger" role="alert">
            <h2 class="alert-heading">Server Error</h2>
            <p class="blockquote">Our backend API is very unstable and it fails from time to time.  When it becomes stable, this page will disappear forever.</p>
            <hr />
            <div>
                <i class="fas fa-bug fa-10x"></i>
            </div>
        </div>
    </div>
@endsection
