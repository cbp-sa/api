@extends(config('template.layout'))

@section('javascript')
    <link rel="stylesheet" href="{{ mix('css/airquest.css') }}">
@endsection

@section('content')
    <div class="airquest container">
        <div class="row">
             <div class="col-md-12">
            <div class="alert" role="alert">
                <h2 class="alert-heading text-center text-capitalize">You have too many failed login attempts</h2>
                <hr />
                <dl class="row">
                    @foreach ($parameters as $index => $parameter)
                        <dt class="col-sm-3 text-right">{{ $index }}&nbsp;:&nbsp;</dt>
                        <dd class="col-sm-9">{{ $parameter }}</dd>
                    @endforeach
                </dl>
                <hr />
                <div class="text-center">
                    <p>Please contact us to resolve this issue on <a href="tel:{{ $telephone }}">{{ $telephone }}</a> or <a href="mailto:{{ $email }}">{{ $email }}</a></p>
                </div>
            </div>
            </div>
        </div>
    </div>
@endsection
