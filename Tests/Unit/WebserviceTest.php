<?php

namespace BureauHouse\Modules\AirQuest\Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use BureauHouse\Modules\AirQuest\Http\WebService;

class WebserviceTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testInstance()
    {
        $this->assertInstanceOf('BureauHouse\Modules\AirQuest\Http\Webservice', Webservice::getInstance());
    }
}
